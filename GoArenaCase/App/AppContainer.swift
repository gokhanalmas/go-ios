//
//  AppContainer.swift
//  GoArenaCase
//
//  Created by KG Teknoloji on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

let app = AppContainer()

final class AppContainer {
    let router = AppRouter()
    // declare network services here( user service, product service etc.)
}
