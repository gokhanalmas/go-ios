//
//  AppRouter.swift
//  GoArenaCase
//
//  Created by KG Teknoloji on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import UIKit

final class AppRouter {
    let window: UIWindow

    init() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
    }

    /// application will start with this functuon. 
    func start() {
        let login = LoginBuilder.make()
//        let taskView = TasksBuilder.make()
        let baseNavController = BaseNavigationController(rootViewController: login)
        window.rootViewController = baseNavController
        window.makeKeyAndVisible()
    }
}
