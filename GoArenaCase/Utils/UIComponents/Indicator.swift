//
//  Indicator.swift
//  GoArenaCase
//
//  Created by Serhat Akalin on 24.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import UIKit
import SVProgressHUD

func needLoading(_ loading: Bool) {
    loading == true ? Indicator.shared.showIndicator() : Indicator.shared.closeIndicator()
}

class Indicator {
    static let shared = Indicator()
     func showIndicator(){
        DispatchQueue.main.async {
            SVProgressHUD.setOffsetFromCenter(UIOffset(horizontal: UIScreen.main.bounds.width/2, vertical: UIScreen.main.bounds.height/2))
            SVProgressHUD.setForegroundColor(UIColor.blue)
            SVProgressHUD.setBackgroundColor(UIColor.clear)
            SVProgressHUD.setBackgroundLayerColor(UIColor.clear)
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
            SVProgressHUD.show()
        }
        
    }
    func closeIndicator(){
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
}
