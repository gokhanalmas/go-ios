//
//  UserDefaultsManager.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

final class UserDefaultsManager {
    static let shared = UserDefaultsManager()

    /// Save Data To UserDefaults
    ///
    /// - Parameters:
    ///   - value: Any value to save
    ///   - key: Private UserDefaultKeys enum
    fileprivate func saveData(value: Any, key: UserDefaultKeys) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }

    /// Get Data From Userdefaults
    ///
    /// - Parameter key: Any value to return
    /// - Returns: Private UserDefaultKeys enum
    fileprivate func getData<T>(key: UserDefaultKeys) -> T? {
        return UserDefaults.standard.object(forKey: key.rawValue) as? T
    }

    func removeAllData() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }

    func saveToken(token: String) {
        saveData(value: token, key: .token)
    }

    func getToken() -> String? {
        return getData(key: .token)
    }

    func saveUserName(userName: String) {
        saveData(value: userName, key: .userName)
    }

    func getUserName() -> String? {
        return getData(key: .userName)
    }
}

extension UserDefaultsManager {
    fileprivate enum UserDefaultKeys: String {
        case token = "id_token"
        case userName = "userName"
    }
}
