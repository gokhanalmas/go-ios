//
//  Storyboards.swift
//  GoArenaCase
//
//  Created by KG Teknoloji on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

/// Holds storyboard identifiers
///
/// - login: LoginViewController
enum AppStoryboard: String {
    case login = "Login"
    case tasks = "Tasks"
    case profile = "Profile"
    //profile, search vs.
}
