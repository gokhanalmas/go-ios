//
//  BaseBuilder.swift
//  GoArenaCase
//
//  Created by KG Teknoloji on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import UIKit

class BaseBuilder: NSObject {}

extension BaseBuilder {

    /// A helper method to create view controller from custom storyboard
    ///
    /// - Parameters:
    ///   - appStoryboard: Custom Storyboard names
    ///   - viewController: view controller identifier
    /// - Returns: UIViewController
    class func instantiate<T: UIViewController>(appStoryboard: AppStoryboard, viewController: String) -> T {
        let storyboard = UIStoryboard(name: appStoryboard.rawValue, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: viewController) as! T
    }
}
