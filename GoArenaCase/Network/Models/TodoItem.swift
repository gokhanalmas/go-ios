//
//  TodoItem.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

struct TodoItem: Decodable {

    let id: Int
    let text: String
    let done: Bool
    let status: String
}
