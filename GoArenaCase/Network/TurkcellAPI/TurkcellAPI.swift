//
//  TurkcellAPI.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 21.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation
import Moya

enum Environment: String {
    case staging
    case production = "http://35.247.127.111:8080/api/"
}

var environment: Environment = .production

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data
    }
}

let wsProvider = MoyaProvider<TurkcellAPI>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

public enum TurkcellAPI {
    case login([String: Any])
    case todo
    case profile(String)
}

extension TurkcellAPI: TargetType {

    public var baseURL: URL {
        return URL(string: environment.rawValue)!
    }

    public var path: String {
        switch self {
        case .login:
            return "authenticate"
        case .todo:
            return "todos"
        case .profile(let userName):
            return "users/\(userName)"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .login:
            return .post
        case .todo:
            return .get
        case .profile:
            return .get
        }
    }

    public var sampleData: Data {
        switch self {
        case .login:
            return Data()
        default:
            return Data()
        }
    }

    public var task: Task {
        switch self {
        case .login(let params):
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        case .todo:
            return .requestPlain
        case .profile:
            return .requestPlain
        }
    }

    public var headers: [String: String]? {
        let token = UserDefaultsManager.shared.getToken()
        if let token = token {
            return ["Authorization": "Bearer \(token)"]
        }
        return nil
    }

}
