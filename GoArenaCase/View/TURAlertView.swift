//
//  TURAlertView.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import UIKit

final class TURAlertView {
    static func createBasicAlert(title: String, message: String, viewController: UIViewController) {
        let alertController = UIAlertController(title: title.localizedString, message: message.localizedString, preferredStyle: .alert)
        let action = UIAlertAction(title: "done".localizedString, style: .default, handler: nil)
        alertController.addAction(action)
        viewController.present(alertController, animated: true, completion: nil)
    }
}
