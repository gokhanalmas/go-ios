//
//  ProfileContracts.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

// View

protocol ProfileViewProtocol: class {
    func handleOutput(_ output: ProfilePresenterOutput)
}

// Presenter

protocol ProfilePresenterProtocol {
    func getUserdetail()
    func logout()
}

enum ProfilePresenterOutput {
    case handleUserDetail(_ user: User?, _ error: Error?)
}

// Interactor

protocol ProfileInteractorProtocol {
    var delegate: ProfileInteractorDelegate? { get set }
    func getUserdetail()
    func logout()
}

protocol ProfileInteractorDelegate: class {
    func handleOutput(_ output: ProfileInteractorOutput)
}

enum ProfileInteractorOutput {
    case handleUserDetail(_ user: User?, _ error: Error?)
    case logout()
}

// Router

enum ProfileRoute: Equatable {
    case login
}

protocol ProfileRouterProtocol {
    func navigate(_ route: ProfileRoute)
}
