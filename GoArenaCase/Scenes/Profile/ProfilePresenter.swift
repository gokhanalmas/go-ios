//
//  ProfilePresenter.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

final class ProfilePresenter {
    unowned var view: ProfileViewProtocol
    private var interactor: ProfileInteractorProtocol
    private let router: ProfileRouterProtocol

    init(view: ProfileViewProtocol, interactor: ProfileInteractorProtocol, router: ProfileRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router

        self.interactor.delegate = self
    }

}

extension ProfilePresenter: ProfilePresenterProtocol {

    func getUserdetail() {
        interactor.getUserdetail()
    }

    func logout() {
        interactor.logout()
    }
}

extension ProfilePresenter: ProfileInteractorDelegate {
    func handleOutput(_ output: ProfileInteractorOutput) {
        switch output {
        case .handleUserDetail(let user, let error):
            view.handleOutput(.handleUserDetail(user, error))
        case .logout:
            router.navigate(.login)
        }
    }
}
