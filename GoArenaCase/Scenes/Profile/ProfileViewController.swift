//
//  ProfileViewController.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 21.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    var presenter: ProfilePresenter!

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.getUserdetail()
    }

    fileprivate func handleUserDetail(user: User?, error: Error?) {
        if error != nil {
            TURAlertView.createBasicAlert(title: "error", message: "generic-error", viewController: self)
        }

        if let user = user {
            self.nameLabel.text = user.firstName + " " + user.lastName
            self.usernameLabel.text = user.email
        }
    }

    @IBAction func logoutTapped(_ sender: Any) {
        presenter.logout()
    }
}

extension ProfileViewController: ProfileViewProtocol {
    func handleOutput(_ output: ProfilePresenterOutput) {
        switch output {
        case .handleUserDetail(let user, let error):
            self.handleUserDetail(user: user, error: error)
        }
    }
}
