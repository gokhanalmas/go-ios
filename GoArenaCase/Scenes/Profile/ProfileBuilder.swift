//
//  ProfileBuilder.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

final class ProfileBuilder: BaseBuilder {
    static func make() -> ProfileViewController {
        let view: ProfileViewController = self.instantiate(appStoryboard: .profile, viewController: "ProfileViewController")
        let interactor = ProfileInteractor()
        let router = ProfileRouter(viewController: view)
        let presenter = ProfilePresenter(view: view, interactor: interactor, router: router)

        view.presenter = presenter

        return view
    }
}
