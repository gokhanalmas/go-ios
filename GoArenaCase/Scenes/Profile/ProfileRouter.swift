//
//  ProfileRouter.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import UIKit

final class ProfileRouter {
    unowned var viewController: UIViewController

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension ProfileRouter: ProfileRouterProtocol {
    func navigate(_ route: ProfileRoute) {
        switch route {
        case .login:
            let login = LoginBuilder.make()
            viewController.show(login, sender: nil)
        }
    }
}
