//
//  ProfileInteractor.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

final class ProfileInteractor {
    weak var delegate: ProfileInteractorDelegate?
}

extension ProfileInteractor: ProfileInteractorProtocol {
    func getUserdetail() {
        needLoading(true)
        guard let userName = UserDefaultsManager.shared.getUserName() else { return }
        wsProvider.request(.profile(userName)) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let value):
                do {
                    let responseData = value.data
                    let apiResponse = try JSONDecoder().decode(User.self, from: responseData)
                    self.delegate?.handleOutput(.handleUserDetail(apiResponse, nil))
                    needLoading(false)
                } catch let error {
                    self.delegate?.handleOutput(.handleUserDetail(nil, error))
                    needLoading(false)
                }
            case .failure(let error):
                self.delegate?.handleOutput(.handleUserDetail(nil, error))
                needLoading(false)
            }
        }
    }

    func logout() {
        UserDefaultsManager.shared.removeAllData()
        self.delegate?.handleOutput(.logout())
    }
}
