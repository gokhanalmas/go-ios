//
//  LoginInteractor.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

final class LoginInteractor {
    weak var delegate: LoginInteractorDelegate?

}

extension LoginInteractor: LoginInteractorProtocol {
    fileprivate func saveUserDefaults(_ apiResponse: LoginResponse, _ userName: String) {
        UserDefaultsManager.shared.saveToken(token: apiResponse.token)
        UserDefaultsManager.shared.saveUserName(userName: userName)
    }

    func login(request : Login.MakeLogin.Request) {
        needLoading(true)
        let params = ["username": request.username,
                      "password": request.password ]
        wsProvider.request(.login(params)) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let value):
                do {
                    let responseData = value.data
                    let apiResponse = try JSONDecoder().decode(LoginResponse.self, from: responseData)
                    self.saveUserDefaults(apiResponse, request.username)
                    self.delegate?.handleOutput(.handleLogin(true, nil))
                    needLoading(false)
                } catch let error {
                    self.delegate?.handleOutput(.handleLogin(false, error))
                    needLoading(false)
                }
            case .failure(let error):
                self.delegate?.handleOutput(.handleLogin(false, error))
                needLoading(false)
            }
        }
    }
}
