//
//  LoginPresenter.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

final class LoginPresenter {
    unowned var view: LoginViewProtocol
    private var interactor: LoginInteractorProtocol
    private let router: LoginRouterProtocol

    init(view: LoginViewProtocol, interactor: LoginInteractorProtocol, router: LoginRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router

        self.interactor.delegate = self
    }

}

extension LoginPresenter: LoginPresenterProtocol {
    func login(request: Login.MakeLogin.Request) {
        interactor.login(request: Login.MakeLogin.Request(username: request.username, password: request.password))
    }
    
  

    func navigate(_ route: LoginRoute) {
        router.navigate(route)
    }

}

extension LoginPresenter: LoginInteractorDelegate {
    func handleOutput(_ output: LoginInteractorOutput) {
        switch output {
        case .handleLogin(let success, let error):
            view.handleOutput(.handleLogin(success, error))
        }
    }
}
