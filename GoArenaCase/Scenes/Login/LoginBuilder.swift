//
//  LoginBuilder.swift
//  GoArenaCase
//
//  Created by KG Teknoloji on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

final class LoginBuilder: BaseBuilder {
    static func make() -> LoginViewController {
        let view: LoginViewController = self.instantiate(appStoryboard: .login, viewController: "LoginViewController")
        let interactor = LoginInteractor()
        let router = LoginRouter(viewController: view)
        let loginPresenter = LoginPresenter(view: view, interactor: interactor, router: router)

        view.presenter = loginPresenter
        return view
    }
}
