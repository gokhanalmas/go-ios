//
//  LoginViewController.swift
//  GoArenaCase
//
//  Created by KG Teknoloji on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    var presenter: LoginPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.text = "user"
        passwordTextField.text = "user"
        self.navigationItem.hidesBackButton = true
    }

    // MARK: IBActions
    @IBAction func loginTapped(_ sender: UIButton) {
        if let username = usernameTextField.text, let password = passwordTextField.text {
            if username.isEmpty || password.isEmpty {
                TURAlertView.createBasicAlert(title: "error", message: "fill-fields", viewController: self)
            } else {
                presenter.login(request: Login.MakeLogin.Request(username: username, password: password))
            }
        }
    }
}

extension LoginViewController: LoginViewProtocol {
    func handleOutput(_ output: LoginPresenterOutput) {
        switch output {
        case .handleLogin(let success, let error):
            handleLogin(success: success, error: error)
        }
    }

    fileprivate func handleLogin(success: Bool, error: Error?) {
        if error != nil {
            TURAlertView.createBasicAlert(title: "error", message: "login-error", viewController: self)
            return
        }

        if success {
            presenter.navigate(.detail)
        }

    }
}
