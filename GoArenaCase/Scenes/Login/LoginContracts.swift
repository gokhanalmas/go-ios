//
//  LoginContracts.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

// View

protocol LoginViewProtocol: class {
    func handleOutput(_ output: LoginPresenterOutput)
}

// Presenter

protocol LoginPresenterProtocol {
    func login(request : Login.MakeLogin.Request)
    func navigate(_ route: LoginRoute)
}

enum LoginPresenterOutput {
    case handleLogin(_ success: Bool, _ error: Error?)
}

// Interactor

protocol LoginInteractorProtocol {
    var delegate: LoginInteractorDelegate? { get set }
    func login(request : Login.MakeLogin.Request)
}

protocol LoginInteractorDelegate: class {
    func handleOutput(_ output: LoginInteractorOutput)
}

enum LoginInteractorOutput {
    case handleLogin(_ success: Bool, _ error: Error?)
}

// Router

enum LoginRoute: Equatable {
    case detail
}

protocol LoginRouterProtocol {
    func navigate(_ route: LoginRoute )
}
