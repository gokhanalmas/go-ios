//
//  LoginModels.swift
//  GoArenaCase
//
//  Created by Serhat Akalin on 24.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

enum Login
{
    enum MakeLogin {
        struct Request {
            var username : String
            var password : String
        }
    }
}
