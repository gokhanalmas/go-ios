//
//  LoginRouter.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import UIKit

final class LoginRouter {
    unowned var viewController: UIViewController

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension LoginRouter: LoginRouterProtocol {
    func navigate(_ route: LoginRoute) {
        switch route {
        case .detail:
            let tasks = TasksBuilder.make()
            viewController.show(tasks, sender: nil)
        }
    }
}
