//
//  TasksBuilder.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

final class TasksBuilder: BaseBuilder {
    static func make() -> TasksTableViewController {
        let view: TasksTableViewController = self.instantiate(appStoryboard: .tasks, viewController: "TasksTableViewController")
        let interactor = TasksInteractor()
        let router = TasksRouter(viewController: view)
        let presenter = TasksPresenter(view: view, interactor: interactor, router: router)

        view.presenter = presenter
        return view
    }
}
