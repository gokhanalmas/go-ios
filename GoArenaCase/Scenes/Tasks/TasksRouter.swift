//
//  TasksRouter.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import UIKit

final class TasksRouter {
    unowned var viewController: UIViewController

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension TasksRouter: TasksRouterProtocol {
    func navigate(_ route: TasksRoute) {
        switch route {
        case .profile:
            let view = ProfileBuilder.make()
            viewController.show(view, sender: nil)
        }
    }
}
