//
//  TasksTableViewController.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 20.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import UIKit

class TasksTableViewController: BaseTableViewController {

    var todoItems = [TodoItem]()
    var presenter: TasksPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
         self.clearsSelectionOnViewWillAppear = true
        presenter.getTodos()
        setupUI()
    }

    fileprivate func setupUI() {
        let barButtonItem = UIBarButtonItem(title: "profile".localizedString, style: .plain, target: self, action: #selector(profileTapped))
        self.navigationItem.rightBarButtonItem = barButtonItem

        self.navigationItem.hidesBackButton = true

    }

    @objc fileprivate func profileTapped() {
        presenter.navigate(.profile)
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoItems.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        let item = todoItems[indexPath.row]
        cell.textLabel?.text = item.text
        cell.detailTextLabel?.text = item.status
        cell.accessoryType = item.done ? .checkmark : .none
        return cell
    }

    fileprivate func handleTodos(_ todos: [TodoItem]?, error: Error?) {
        if error != nil {
            TURAlertView.createBasicAlert(title: "error", message: "generic-error", viewController: self)
        }

        if let todos = todos {
            self.todoItems = todos
            self.tableView.reloadData()
        }
    }

}

extension TasksTableViewController: TasksViewProtocol {
    func handleOutput(_ output: TasksPresenterOutput) {
        switch output {
        case .handleTodos(let todo, let error):
            self.handleTodos(todo, error: error)
        }
    }

}
