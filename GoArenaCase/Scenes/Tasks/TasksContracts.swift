//
//  TasksContracts.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation
// View

protocol TasksViewProtocol: class {
    func handleOutput(_ output: TasksPresenterOutput)
}

// Presenter

protocol TasksPresenterProtocol {
    func getTodos()
    func navigate(_ route: TasksRoute)
}

enum TasksPresenterOutput {
    case handleTodos(_ items: [TodoItem]?, _ error: Error?)
}

// Interactor

protocol TasksInteractorProtocol {
    var delegate: TasksInteractorDelegate? { get set }
    func getTodos()
}

protocol TasksInteractorDelegate: class {
    func handleOutput(_ output: TasksInteractorOutput)
}

enum TasksInteractorOutput {
    case handleTodos(_ items: [TodoItem]?, _ error: Error?)
}

// Router

enum TasksRoute: Equatable {
    case profile
}

protocol TasksRouterProtocol {
    func navigate(_ route: TasksRoute )
}
