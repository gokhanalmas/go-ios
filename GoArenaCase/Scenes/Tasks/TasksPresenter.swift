//
//  TasksPresenter.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

final class TasksPresenter {
    unowned var view: TasksViewProtocol
    private var interactor: TasksInteractorProtocol
    private let router: TasksRouterProtocol

    init(view: TasksViewProtocol, interactor: TasksInteractorProtocol, router: TasksRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router

        self.interactor.delegate = self
    }

}

extension TasksPresenter: TasksPresenterProtocol {

    func getTodos() {
        interactor.getTodos()
    }

    func navigate(_ route: TasksRoute) {
        router.navigate(route)
    }

}

extension TasksPresenter: TasksInteractorDelegate {
    func handleOutput(_ output: TasksInteractorOutput) {
        switch output {
        case .handleTodos(let todos, let error):
            view.handleOutput(.handleTodos(todos, error))
        }
    }

}
