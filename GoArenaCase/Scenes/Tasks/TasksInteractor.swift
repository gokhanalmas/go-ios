//
//  TasksInteractor.swift
//  GoArenaCase
//
//  Created by Azat Göktaş on 22.03.2019.
//  Copyright © 2019 KG Teknoloji. All rights reserved.
//

import Foundation

final class TasksInteractor {
    weak var delegate: TasksInteractorDelegate?
}

extension TasksInteractor: TasksInteractorProtocol {

    func getTodos() {
        wsProvider.request(.todo) { [weak self] (result) in
            
            guard let self = self else { return }
            switch result {
            case .success(let value):
                do {
                    let responseData = value.data
                    let apiResponse = try JSONDecoder().decode([TodoItem].self, from: responseData)
                    self.delegate?.handleOutput(.handleTodos(apiResponse, nil))
                } catch let error {
                    self.delegate?.handleOutput(.handleTodos(nil, error))
                }
            case .failure(let error):
                self.delegate?.handleOutput(.handleTodos(nil, error))
            }
        }
    }
}
